package co.enrike.registrohoras.registrohoras.interfaces;


import co.enrike.registrohoras.registrohoras.fragments.BienvenidaFragment;
import co.enrike.registrohoras.registrohoras.fragments.ConsultaListaUsuarioImagenUrlFragment;
import co.enrike.registrohoras.registrohoras.fragments.ConsultaUsuarioUrlFragment;
import co.enrike.registrohoras.registrohoras.fragments.ConsultarListaUsuariosFragment;
import co.enrike.registrohoras.registrohoras.fragments.DesarrolladorFragment;
import co.enrike.registrohoras.registrohoras.fragments.RegistrarUsuarioFragment;

/**
 * Created by CHENAO on 5/08/2017.
 */

public interface IFragments extends BienvenidaFragment.OnFragmentInteractionListener,DesarrolladorFragment.OnFragmentInteractionListener,
        RegistrarUsuarioFragment.OnFragmentInteractionListener,
        ConsultarListaUsuariosFragment.OnFragmentInteractionListener,
        ConsultaUsuarioUrlFragment.OnFragmentInteractionListener,ConsultaListaUsuarioImagenUrlFragment.OnFragmentInteractionListener{
}
