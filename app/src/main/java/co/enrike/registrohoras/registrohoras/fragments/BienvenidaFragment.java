package co.enrike.registrohoras.registrohoras.fragments;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.enrike.registrohoras.registrohoras.DatabaseHelper;
import co.enrike.registrohoras.registrohoras.R;


public class BienvenidaFragment extends Fragment {

    View rootView;
    DatabaseHelper mDBHelper;
    SQLiteDatabase db;
    EditText edt_ingreso, edt_salida, fechaHoy;
    TextView fecha;
    Button guardar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_bienvenida, container, false);
        fecha = (TextView) rootView.findViewById(R.id.lblFechaHoy);
        edt_ingreso = (EditText) rootView.findViewById(R.id.edt_ingreso);
        edt_salida = (EditText) rootView.findViewById(R.id.edt_salida);
        fechaHoy = (EditText) rootView.findViewById(R.id.txtFechaHoy);
        guardar = (Button) rootView.findViewById(R.id.btnGuardar);
        mDBHelper = new DatabaseHelper(getContext());
        //SACAMOS LA FECHA COMPLETA
        Date d = new Date();
        SimpleDateFormat fecc = new SimpleDateFormat("d, MMMM 'del' yyyy");
        String fechacComplString = fecc.format(d);
        fecha.setText(fechacComplString);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edt_ingreso.getText().toString().trim().isEmpty() && !edt_salida.getText().toString().trim().isEmpty() && !fechaHoy.getText().toString().trim().isEmpty()) {

                    GuadarIngreso();

                }
            }
        });
        return rootView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_principal, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_caphora) {

            Date d = new Date();
            SimpleDateFormat hoIngreso = new SimpleDateFormat("hh:mm a");
            String horaString = hoIngreso.format(d);

            if (edt_ingreso.getText().toString().trim().isEmpty()) {


                edt_ingreso.setText(horaString);

            } else {

                edt_salida.setText(horaString);

            }

        }


        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionListener {
    }


    private void GuadarIngreso() {
        try {

            String horaIngreso = String.valueOf(edt_ingreso.getText().toString().charAt(0)) + String.valueOf(edt_ingreso.getText().toString().charAt(1) + "." + String.valueOf(edt_ingreso.getText().toString().charAt(3)) + String.valueOf(edt_ingreso.getText().toString().charAt(4)));
            String horaSalida = String.valueOf(edt_salida.getText().toString().charAt(0)) + String.valueOf(edt_salida.getText().toString().charAt(1) + "." + String.valueOf(edt_salida.getText().toString().charAt(3)) + String.valueOf(edt_salida.getText().toString().charAt(4)));
            double totalHoras = Double.parseDouble(horaSalida) - Double.parseDouble(horaIngreso);

            db = mDBHelper.getWritableDatabase();
            String sql = "insert into registrodia (horaingreso,horasalida,totalhoras,fecha)values (?,?,?,?)";
            SQLiteStatement insert = db.compileStatement(sql);
            insert.clearBindings();
            insert.bindString(1, edt_ingreso.getText().toString().trim());
            insert.bindString(2, edt_salida.getText().toString().trim());
            insert.bindString(3, String.format("%.2f", totalHoras));
            insert.bindString(4, fechaHoy.getText().toString().trim());
            insert.executeInsert();

            Toast.makeText(getContext(), "Registro Guardado ", Toast.LENGTH_SHORT).show();

        } catch (Exception ex) {
            Toast.makeText(getContext(), String.valueOf(ex), Toast.LENGTH_SHORT).show();
        }
    }
}
