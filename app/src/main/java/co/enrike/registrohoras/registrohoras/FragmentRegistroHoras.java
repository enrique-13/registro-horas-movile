package co.enrike.registrohoras.registrohoras;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FragmentRegistroHoras extends Fragment {

    View rootView;
    DatabaseHelper mDBHelper;
    SQLiteDatabase db;
    TextView lista;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_registro_horas, container, false);
        lista = (TextView) rootView.findViewById(R.id.lblListaRegistros);
        mDBHelper = new DatabaseHelper(getContext());
        BuscarRegistro();
        return rootView;
    }

    private void BuscarRegistro() {
        try {

            db = mDBHelper.getWritableDatabase();
            String sql = "select * from registrodia";
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                lista.setText(null);
                do {

                    String horaIngreso = cursor.getString(1);
                    String horaSalida = cursor.getString(2);
                    String horaTotal = cursor.getString(3);
                    String fecha = cursor.getString(4);
                    lista.append(fecha + "          " + horaIngreso + "                 " + horaSalida + "        " + horaTotal + "\n" + "\n");
                } while (cursor.moveToNext());

            }
        } catch (Exception ex) {
        }
    }

}
