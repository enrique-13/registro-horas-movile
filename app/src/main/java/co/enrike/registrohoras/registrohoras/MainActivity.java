package co.enrike.registrohoras.registrohoras;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import co.enrike.registrohoras.registrohoras.fragments.BienvenidaFragment;
import co.enrike.registrohoras.registrohoras.fragments.ConsultaListaUsuarioImagenUrlFragment;
import co.enrike.registrohoras.registrohoras.fragments.ConsultaUsuarioUrlFragment;
import co.enrike.registrohoras.registrohoras.fragments.DesarrolladorFragment;
import co.enrike.registrohoras.registrohoras.fragments.RegistrarUsuarioFragment;
import co.enrike.registrohoras.registrohoras.interfaces.IFragments;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IFragments {

    DatabaseHelper mDBHelper;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDBHelper = new DatabaseHelper(getApplicationContext());

        //Check exists database
        File database = getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        if (false == database.exists()) {
            mDBHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(this)) {
                Toast.makeText(this, "Copy database succes", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Fragment miFragment = new BienvenidaFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, miFragment).commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity", "DB copied");
            return true;
        } catch (Exception e) {
            Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();

            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment miFragment = null;
        boolean fragmentSeleccionado = false;

        if (id == R.id.nav_inicio) {
            miFragment = new BienvenidaFragment();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_registro) {
            miFragment = new RegistrarUsuarioFragment();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_registro_horas) {
            miFragment = new FragmentRegistroHoras();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_consulta_usuario) {
            miFragment = new ConsultaUsuarioUrlFragment();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_loca) {
            miFragment = new LocalizacionFragment();
            fragmentSeleccionado = true;
        } else if (id == R.id.nav_lista_usuario) {

            miFragment = new ConsultaListaUsuarioImagenUrlFragment();
            fragmentSeleccionado = true;

        } else if (id == R.id.nav_desarrollador) {
            miFragment = new DesarrolladorFragment();
            fragmentSeleccionado = true;
        }

        if (fragmentSeleccionado == true) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, miFragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
